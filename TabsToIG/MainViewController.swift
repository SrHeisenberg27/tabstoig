//
//  MainViewController.swift
//  TabsToIG
//
//  Created by Álvaro Ferrández Gómez on 13/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class MainViewController: CustomTabsController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavBar()
        self.setTabs()
    }

    fileprivate func setNavBar() {
        navigationItem.title = "TabsToIG"
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.barTintColor = .systemRed
        navigationController?.navigationBar.isTranslucent = false
    }

    fileprivate func setTabs() {
        addItem(item: ExampleVC1(), title: "Uno")
        addItem(item: ExampleVC2(), title: "Dos")
        addItem(item: ExampleVC3(), title: "Tres"
        )

        if let exampleVC4 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ExampleVC4") as? ExampleVC4 {
            addItem(item: exampleVC4, title: "Cuatro")
        }

        setHeaderActiveColor(color: .white)
        setHeaderInActiveColor(color: .lightGray)
        setHeaderBackgroundColor(color: .systemRed)

        setCurrentPosition(position: 0)
        setStyle(style: .fixed)

        build()
    }
}

