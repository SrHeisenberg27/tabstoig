//
//  HeaderCell.swift
//  TabsToIG
//
//  Created by Álvaro Ferrández Gómez on 13/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

public class HeaderCell: UICollectionViewCell {

    public let label = UILabel()
    public let indicator = UIView()

    public var text: String? {
        didSet {
            self.label.text = text
        }
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func select(didSelect: Bool, activeColor: UIColor, inActiveColor: UIColor) {
        self.indicator.backgroundColor = activeColor

        if didSelect {
            self.label.textColor = activeColor
            self.indicator.isHidden = false
        } else {
            self.label.textColor = inActiveColor
            self.indicator.isHidden = true
        }
    }

    public func setupUI() {
        self.addSubview(label)
        self.addSubview(indicator)

        self.label.translatesAutoresizingMaskIntoConstraints = false
        self.label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        self.label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.label.font = UIFont.boldSystemFont(ofSize: 18)

        self.indicator.translatesAutoresizingMaskIntoConstraints = false
        self.indicator.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.indicator.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.indicator.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.indicator.heightAnchor.constraint(equalToConstant: 2).isActive = true
    }
}
