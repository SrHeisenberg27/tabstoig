//
//  CustomTabsController.swift
//  TabsToIG
//
//  Created by Álvaro Ferrández Gómez on 13/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

public enum CustomTabsControllerType: String {
    case fixed
    case flexible
}

public class CustomTabsController: UIViewController {

    public let collectionHeader = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    public let collectionPage = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())

    public let collectionHeaderIdentifier = "COLLECTION_HEADER_IDENTIFIER"
    public let collectionPageIdentifier = "COLLECTION_PAGE_IDENTIFIER"
    public var items = [UIViewController]()
    public var titles = [String]()

    public var colorHeaderActive = UIColor.blue
    public var colorHeaderInActive = UIColor.gray
    public var colorHeaderBackground = UIColor.white

    public var currentPosition = 0
    public var tabStyle = CustomTabsControllerType.fixed
    public let heightHeader = 57

    public func addItem(item: UIViewController, title: String) {
        items.append(item)
        titles.append(title)
    }

    public func setHeaderBackgroundColor(color: UIColor) {
        colorHeaderBackground = color
    }

    public func setHeaderActiveColor(color: UIColor) {
        colorHeaderActive = color
    }

    public func setHeaderInActiveColor(color: UIColor) {
        colorHeaderInActive = color
    }

    public func setCurrentPosition(position: Int) {
        currentPosition = position
        let path = IndexPath(item: currentPosition, section: 0)

        DispatchQueue.main.async {
            if self.tabStyle == .flexible {
                self.collectionHeader.scrollToItem(at: path, at: .centeredHorizontally, animated: true)
            }
            self.collectionHeader.reloadData()
        }

        DispatchQueue.main.async {
            self.collectionPage.scrollToItem(at: path, at: .centeredHorizontally, animated: true)
        }
    }

    public func setStyle(style: CustomTabsControllerType) {
        self.tabStyle = style
    }

    public func build() {
        self.view.addSubview(collectionHeader)
        self.view.addSubview(collectionPage)

        self.collectionHeader.translatesAutoresizingMaskIntoConstraints = false
        self.collectionHeader.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.collectionHeader.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        self.collectionHeader.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        self.collectionHeader.heightAnchor.constraint(equalToConstant: CGFloat(heightHeader)).isActive = true
        (collectionHeader.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = .horizontal
        self.collectionHeader.showsHorizontalScrollIndicator = false
        self.collectionHeader.backgroundColor = colorHeaderBackground
        self.collectionHeader.register(HeaderCell.self, forCellWithReuseIdentifier: collectionHeaderIdentifier)
        self.collectionHeader.delegate = self
        self.collectionHeader.dataSource = self
        self.collectionHeader.reloadData()

        self.collectionPage.translatesAutoresizingMaskIntoConstraints = false
        self.collectionPage.topAnchor.constraint(equalTo: collectionHeader.bottomAnchor).isActive = true
        self.collectionPage.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        self.collectionPage.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        self.collectionPage.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        self.collectionPage.backgroundColor = .black
        self.collectionPage.showsHorizontalScrollIndicator = false
        (collectionPage.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = .horizontal
        self.collectionPage.isPagingEnabled = true
        self.collectionPage.register(UICollectionViewCell.self, forCellWithReuseIdentifier: collectionPageIdentifier)
        self.collectionPage.delegate = self
        self.collectionPage.bounces = false
        self.collectionPage.dataSource = self
        self.collectionPage.reloadData()
    }
}

extension CustomTabsController: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.setCurrentPosition(position: indexPath.row)
        self.collectionHeader.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }

    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collectionPage {
            let currentIndex = Int(self.collectionPage.contentOffset.x / collectionPage.frame.size.width)
            self.setCurrentPosition(position: currentIndex)
            self.collectionHeader.scrollToItem(at: IndexPath(row: currentIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
}

extension CustomTabsController: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionHeader {
            return titles.count
        }

        return items.count
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionHeader {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionHeaderIdentifier, for: indexPath) as? HeaderCell {
                cell.text = titles[indexPath.row]

                var didSelect = false

                if currentPosition == indexPath.row {
                    didSelect = true
                }

                cell.select(didSelect: didSelect, activeColor: colorHeaderActive, inActiveColor: colorHeaderInActive)

                return cell
            }
        }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionPageIdentifier, for: indexPath)
        let vc = items[indexPath.row]

        cell.addSubview(vc.view)

        vc.view.translatesAutoresizingMaskIntoConstraints = false
        vc.view.topAnchor.constraint(equalTo: cell.topAnchor, constant: 28).isActive = true
        vc.view.leadingAnchor.constraint(equalTo: cell.leadingAnchor).isActive = true
        vc.view.trailingAnchor.constraint(equalTo: cell.trailingAnchor).isActive = true
        vc.view.bottomAnchor.constraint(equalTo: cell.bottomAnchor).isActive = true

        return cell
    }
}

extension CustomTabsController: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionHeader {
            if tabStyle == .fixed {
                let spacer = CGFloat(titles.count)
                return CGSize(width: view.frame.width / spacer, height: CGFloat(heightHeader))
            } else {
                return CGSize(width: view.frame.width * 20 / 100, height: CGFloat(heightHeader))
            }
        }

        return CGSize(width: view.frame.width, height: view.frame.height)
    }

    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionHeader {
            return 10
        }
        
        if collectionPage == collectionPage{
            return 0
        }

        return 0
    }
}
