//
//  ExampleVC2.swift
//  TabsToIG
//
//  Created by Álvaro Ferrández Gómez on 13/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class ExampleVC2: UIViewController {

    let label = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }

    private func setupUI() {
        self.view.backgroundColor = .lightGray
        self.view.addSubview(label)

        self.label.translatesAutoresizingMaskIntoConstraints = false
        self.label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

        self.label.text = "Tab Dos, por código"
    }
}

