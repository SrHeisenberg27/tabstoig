//
//  ExampleVC4.swift
//  TabsToIG
//
//  Created by Álvaro Ferrández Gómez on 13/06/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

class ExampleVC4: UIViewController {

    @IBOutlet public weak var exampleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }

    private func setupUI() {
        self.view.backgroundColor = .lightGray

        self.exampleLabel.text = "Esta está hecha por Storyboard"
        self.exampleLabel.textColor = .white
    }
}

